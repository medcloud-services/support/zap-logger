package zapLoger

import (
	"encoding/json"
	"os"
	"sync"

	"github.com/natefinch/lumberjack"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	Sug  *zap.SugaredLogger
	once sync.Once
)

type LoggerConf struct {
	Mode           string
	Path           string
	Name           string
	MaxSizeMb      int
	MaxBackupFiles int
	MaxAgeDays     int
	Compress       bool
}

// Инициализировать Zap логгер
func Init(conf LoggerConf) {
	once.Do(func() {
		// Проверяем путь. Создаем в случае отсутствия)
		checkPath(conf.Path, conf.Name)

		// Инициируем логер
		initLogger(&conf)
	})
}

func initLogger(conf *LoggerConf) {
	var logger *zap.Logger

	if conf.Mode == "release" {
		writerSyncer := getLogWriter(conf)

		core := zapcore.NewCore(
			zapcore.NewJSONEncoder(zapcore.EncoderConfig{
				TimeKey:     "ts",
				LevelKey:    "lvl",
				NameKey:     "logger",
				MessageKey:  "msg",
				LineEnding:  zapcore.DefaultLineEnding,
				EncodeLevel: zapcore.LowercaseLevelEncoder,
				EncodeTime:  zapcore.ISO8601TimeEncoder,
			}),
			writerSyncer,
			zapcore.InfoLevel,
		)
		logger = zap.New(core, zap.AddCaller())
	} else {
		rawJSON := []byte(
			`{
	  			"level": "debug",
	  			"encoding": "json",
	  			"outputPaths": ["stdout", "` + conf.Path + conf.Name + `"],
	  			"errorOutputPaths": ["stderr", "` + conf.Path + conf.Name + `"],
				"encoderConfig": {
	    			"messageKey": "msm",
	    			"levelKey": "lvl",
	    			"levelEncoder": "lowercase",
        			"timeKey": "ts",
	    			"timeEncoder": "ISO8601"
	  			}
			}`)

		var cfg zap.Config
		if err := json.Unmarshal(rawJSON, &cfg); err != nil {
			panic(err)
		}

		var err error
		logger, err = cfg.Build()
		if err != nil {
			panic(err)
		}
	}
	Sug = logger.Sugar()
}

func getLogWriter(conf *LoggerConf) zapcore.WriteSyncer {
	lumberJackLogger := &lumberjack.Logger{
		Filename:   conf.Path + conf.Name,
		MaxSize:    conf.MaxSizeMb,
		MaxBackups: conf.MaxBackupFiles,
		MaxAge:     conf.MaxAgeDays,
		Compress:   conf.Compress,
	}

	return zapcore.AddSync(lumberJackLogger)
}

func checkPath(path, name string) {
	if path == "" {
		panic("Not set: " + name)
	}

	if _, err := os.Stat(path); os.IsNotExist(err) {
		errDir := os.Mkdir(path, 0o755)
		if errDir != nil {
			panic(errDir)
		}
	}
}
