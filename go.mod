module gitlab.com/medcloud-services/support/zap-logger

go 1.16

require (
	github.com/natefinch/lumberjack v2.0.0+incompatible
	go.uber.org/zap v1.19.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
	mvdan.cc/gofumpt v0.1.1 // indirect
	github.com/golangci/golangci-lint v1.42.0 // indirect
)